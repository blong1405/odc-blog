/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Nov 13, 2015 2:09:39 PM                     ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.odcblog.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedOdcblogConstants
{
	public static final String EXTENSIONNAME = "odcblog";
	public static class TC
	{
		public static final String ENTRY = "Entry".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedOdcblogConstants()
	{
		// private constructor
	}
	
	
}
