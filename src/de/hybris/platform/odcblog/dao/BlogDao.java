package de.hybris.platform.odcblog.dao;

import de.hybris.platform.odcblog.model.EntryModel;

import java.util.List;


/**
 * @author hieu.nguyen
 *
 */
public interface BlogDao
{
	/**
	 *
	 * @return
	 */
	List<EntryModel> getListBlogData();

	/**
	 *
	 * @return
	 */
	List<EntryModel> getBlogDataByEntryCode(String entryCode);

	/**
	 *
	 * @param entryData
	 */
	void deleteBlogDataByEntryCode(String entryCode);

}
