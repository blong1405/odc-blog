/**
 *
 */
package de.hybris.platform.odcblog.dao.impl;

import de.hybris.platform.odcblog.dao.BlogDao;
import de.hybris.platform.odcblog.model.EntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component(value = "blogDao")
public class BlogDaoImpl implements BlogDao
{
	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Autowired
	private ModelService modelService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.odcblog.facades.BlogFacade#getListBlogData()
	 */
	@Override
	public List<EntryModel> getListBlogData()
	{
		// Build a query for the flexible search.
		final String queryString = //
		"SELECT {p:" + EntryModel.PK + "} "//
				+ "FROM {" + EntryModel._TYPECODE + " AS p} ";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);

		return flexibleSearchService.<EntryModel> search(query).getResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.odcblog.dao.BlogDao#getBlogDataByEntry(java.lang.String)
	 */
	@Override
	public List<EntryModel> getBlogDataByEntryCode(final String entryCode)
	{
		final String queryString = //
		"SELECT {p:" + EntryModel.PK + "}" //
				+ "FROM {" + EntryModel._TYPECODE + " AS p} "//
				+ "WHERE " + "{p:" + EntryModel.CODE + "}=?entryCode ";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter("entryCode", entryCode);

		return flexibleSearchService.<EntryModel> search(query).getResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.odcblog.dao.BlogDao#deleteBlogData(java.lang.String)
	 */
	@Override
	public void deleteBlogDataByEntryCode(final String entryData)
	{
		final String queryString = //
		"SELECT {p:" + EntryModel.PK + "}" //
				+ "FROM {" + EntryModel._TYPECODE + " AS p} "//
				+ "WHERE " + "{p:" + EntryModel.CODE + "}=?entryCode ";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter("entryCode", entryData);

		final EntryModel entryModel = flexibleSearchService.<EntryModel> search(query).getResult().get(0);
		modelService.remove(entryModel);
	}

}