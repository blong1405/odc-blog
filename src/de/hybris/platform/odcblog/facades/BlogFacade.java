package de.hybris.platform.odcblog.facades;

import de.hybris.platform.odcblog.data.BlogData;

import java.util.List;


/**
 * @author hieu.nguyen
 *
 */
public interface BlogFacade
{
	/**
	 *
	 * @return
	 */
	List<BlogData> getListBlogData();

	/**
	 *
	 * @return
	 */
	BlogData getBlogDataByEntryCode(String entryCode);

	/**
	 *
	 * @param entryData
	 */
	void deleteBlogDataByEntryCode(String entryCode);

}
