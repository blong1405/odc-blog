/**
 *
 */
package de.hybris.platform.odcblog.facades.impl;

import de.hybris.platform.odcblog.data.BlogData;
import de.hybris.platform.odcblog.facades.BlogFacade;
import de.hybris.platform.odcblog.model.EntryModel;
import de.hybris.platform.odcblog.service.BlogService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


public class BlogFacadeImpl implements BlogFacade
{

	private BlogService blogService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.odcblog.facades.BlogFacade#getListBlogData()
	 */
	@Override
	public List<BlogData> getListBlogData()
	{
		final List<EntryModel> listEntryModel = blogService.getListBlogData();
		final List<BlogData> listBlogData = new ArrayList<BlogData>();

		for (final EntryModel entryModel : listEntryModel)
		{
			final BlogData blogData = new BlogData();
			blogData.setCode(entryModel.getCode());
			blogData.setAuthor(entryModel.getAuthor());
			listBlogData.add(blogData);
		}
		return listBlogData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.odcblog.facades.BlogFacade#getBlogDataByEntryCode(java.lang.String)
	 */
	@Override
	public BlogData getBlogDataByEntryCode(final String entryCode)
	{
		EntryModel entry = null;

		if (entryCode != null)
		{
			entry = blogService.getBlogDataByEntryCode(entryCode);
			if (entry == null)
			{
				return null;
			}
		}
		else
		{
			throw new IllegalArgumentException("Stadium with name " + entryCode + " not found.");
		}
		final BlogData blogData = new BlogData();
		blogData.setCode(entry.getCode());
		blogData.setAuthor(entry.getAuthor().toString());
		blogData.setContent(entry.getContent().toString());
		return blogData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.odcblog.facades.BlogFacade#deleteBlogDataByEntryCode(java.lang.String)
	 */
	@Override
	public void deleteBlogDataByEntryCode(final String entryCode)
	{
		blogService.deleteBlogDataByEntryCode(entryCode);
	}

	@Required
	public void setBlogService(final BlogService blogService)
	{
		this.blogService = blogService;
	}

}