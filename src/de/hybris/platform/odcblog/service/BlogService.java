package de.hybris.platform.odcblog.service;

import de.hybris.platform.odcblog.model.EntryModel;

import java.util.List;


/**
 * @author hieu.nguyen
 *
 */
public interface BlogService
{
	/**
	 *
	 * @return
	 */
	List<EntryModel> getListBlogData();

	/**
	 *
	 * @return
	 */
	EntryModel getBlogDataByEntryCode(String entryCode);

	/**
	 *
	 * @param entryData
	 */
	void deleteBlogDataByEntryCode(String entryCode);

}
