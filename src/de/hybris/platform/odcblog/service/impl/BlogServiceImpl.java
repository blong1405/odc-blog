/**
 *
 */
package de.hybris.platform.odcblog.service.impl;

import de.hybris.platform.odcblog.dao.BlogDao;
import de.hybris.platform.odcblog.model.EntryModel;
import de.hybris.platform.odcblog.service.BlogService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


public class BlogServiceImpl implements BlogService
{

	private BlogDao blogDao;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.odcblog.facades.BlogFacade#getListBlogData()
	 */
	@Override
	public List<EntryModel> getListBlogData()
	{
		final List<EntryModel> listBlogData = blogDao.getListBlogData();
		return listBlogData;
	}

	@Override
	public EntryModel getBlogDataByEntryCode(final String code) throws AmbiguousIdentifierException, UnknownIdentifierException
	{
		final List<EntryModel> result = blogDao.getBlogDataByEntryCode(code);
		if (result.isEmpty())
		{
			throw new UnknownIdentifierException("Blog with code '" + code + "' not found!");
		}
		else if (result.size() > 1)
		{
			throw new AmbiguousIdentifierException("Blog code '" + code + "' is not unique, " + result.size() + " blogs found!");
		}
		return result.get(0);
	}

	@Required
	public void setBlogDao(final BlogDao blogDao)
	{
		this.blogDao = blogDao;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.odcblog.service.BlogService#deleteBlogDataByEntryCode(java.lang.String)
	 */
	@Override
	public void deleteBlogDataByEntryCode(final String entryCode)
	{
		blogDao.deleteBlogDataByEntryCode(entryCode);
	}

}