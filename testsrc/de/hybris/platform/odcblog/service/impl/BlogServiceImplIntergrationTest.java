package de.hybris.platform.odcblog.service.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.platform.odcblog.dao.BlogDao;
import de.hybris.platform.odcblog.model.EntryModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;


/**
 * @author hieu.nguyen
 *
 */
public class BlogServiceImplIntergrationTest extends ServicelayerTransactionalTest
{
	/** As this is an integration test the test to class gets injected here. */
	@Resource
	private BlogDao blogDao;

	/** ModelService used for creation of test data. */
	@Resource
	private ModelService modelService;

	@Test
	public void blogDaoTest()
	{
		//First get one object
		List<EntryModel> listDataByEntryCode = blogDao.getBlogDataByEntryCode("JAVA");
		assertEquals(1, listDataByEntryCode.size());

		//Second list objects
		final List<EntryModel> listBlogData = blogDao.getListBlogData();
		assertEquals(4, listBlogData.size());

		//Third save objects
		final EntryModel entryModel = new EntryModel();
		entryModel.setCode("PHP");
		entryModel.setAuthor("HIEU");
		modelService.save(entryModel);

		//Four verify latest entry save to DB
		listDataByEntryCode = blogDao.getBlogDataByEntryCode("PHP");

		assertEquals("Find the one we just saved", "PHP", listDataByEntryCode.get(0).getCode());
		assertEquals("Find the one we just saved", "HIEU", listDataByEntryCode.get(0).getAuthor());

		//Fifth delete the latest
		//		modelService.remove("PHP");
	}
}
