/**
 *
 */
package de.hybris.platform.odcblog.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.platform.odcblog.dao.BlogDao;
import de.hybris.platform.odcblog.model.EntryModel;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


/**
 * @author luca.gennari
 *
 */
public class BlogServiceImplUnitTest
{
	private BlogServiceImpl blogService;
	private BlogDao blogDao;

	private EntryModel entryModel;

	@Before
	public void setUp()
	{
		blogService = new BlogServiceImpl();
		blogDao = mock(BlogDao.class);
		blogService.setBlogDao(blogDao);

		entryModel = new EntryModel();
		entryModel.setCode("blogCode");
		entryModel.setAuthor("blog Author");
	}

	@Test
	public void testGetAllStadium()
	{
		final List<EntryModel> entryModels = Arrays.asList(entryModel);
		when(blogDao.getListBlogData()).thenReturn(entryModels);
		final List<EntryModel> result = blogService.getListBlogData();
		assertEquals("We should find one", 1, result.size());
	}

}
