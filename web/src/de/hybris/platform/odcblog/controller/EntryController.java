/**
 *
 */
package de.hybris.platform.odcblog.controller;

import de.hybris.platform.odcblog.EntriesNameEncoded;
import de.hybris.platform.odcblog.data.BlogData;
import de.hybris.platform.odcblog.facades.BlogFacade;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
//import java.net.URLDecoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author long.do
 *
 */
@Controller
public class EntryController
{
	private BlogFacade blogFacade;


	@RequestMapping(value = "/blogs")
	public String showStadiums(final Model model)
	{
		final List<BlogData> blogs = blogFacade.getListBlogData();
		model.addAttribute("blogs", blogs);
		return "BlogListing";
	}

	@RequestMapping(value = "/blogs/{blogName}")
	public String showStadiumDetails(@PathVariable String blogName, final Model model) throws UnsupportedEncodingException
	{
		blogName = URLDecoder.decode(blogName, "UTF-8");
		final BlogData blog = blogFacade.getBlogDataByEntryCode(blogName);
		blog.setCode(EntriesNameEncoded.getNameEncoded(blog.getCode()));
		model.addAttribute("blog", blog);
		return "BlogDetails";
	}

	@Autowired
	public void setFacade(final BlogFacade facade)
	{
		this.blogFacade = facade;
	}

}
