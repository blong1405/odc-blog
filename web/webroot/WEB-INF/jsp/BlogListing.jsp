<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <title>Blog Listing</title>
    <body>
        <h1>Blog Listing</h1>
     <ul>
     <c:forEach var="blog" items="${blogs}">
        <li><a href="./blogs/${blog.code}">${blog.code}</a></li>
      </c:forEach>
      </ul>
    </body>
</html>